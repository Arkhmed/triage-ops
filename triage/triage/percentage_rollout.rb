# frozen_string_literal: true

require 'digest'

module Triage
  # Allow processors to be rolled-out gradually based on a given threshold value.
  #
  # The concern shouldn't be included directly, but instead it can be enabled in a processor with
  #
  #   percentage_rollout threshold: 20, on: ->(event) { event.id.to_s }
  #
  # It requires a string event identifier returned by a lambda passed to the `on:` argument, that
  # is used to seed a random integer and compare it against the threshold.
  #
  # The `rollout.rolled_out?(event)` method should be called in the `#applicable?` method of the processor.
  module PercentageRollout
    def percentage_rollout(threshold:, on:)
      define_method(:rollout) do
        @rollout ||= PercentageRollout::Rollout.new(threshold: threshold, identifier_lambda: on)
      end
    end

    class Rollout
      attr_reader :threshold, :identifier_lambda

      def initialize(threshold:, identifier_lambda:)
        @threshold = threshold.to_i
        @identifier_lambda = identifier_lambda
      end

      def rolled_out?(event)
        return false if threshold < 1

        score(event) <= threshold
      end

      private

      def score(event)
        @score ||= Random.new(seed(event)).rand(100)
      end

      def seed(event)
        @seed ||= Digest::MD5.hexdigest(identifier(event)).to_i(16)
      end

      def identifier(event)
        @identifier ||= identifier_lambda.call(event).to_s
      end
    end
  end
end
