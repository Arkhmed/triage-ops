# frozen_string_literal: true

require_relative '../../../lib/constants/labels'
require_relative '../../triage/processor'

module Triage
  class RemoveThreatInsightsTeamLabels < Processor
    react_to 'issue.update'

    def applicable?
      event.from_part_of_product_project? &&
        group_threat_insights_removed? &&
        has_team_label?
    end

    def process
      remove_team_labels
    end

    def documentation
      <<~TEXT
      Remove Threat Insights team labels from an issue that is not assigned to the Threat Insights
      group via the #{Labels::THREAT_INSIGHTS_GROUP_LABEL} label.

      The scoped labels for the Threat Insights team are only relevant within this group. As
      discussed in the 15.4 retrospective:

      https://gitlab.com/gl-retrospectives/govern-sub-department/threat-insights/-/issues/25#note_1102784822
      TEXT
    end

    private

    def group_threat_insights_removed?
      event.removed_label_names.include?(Labels::THREAT_INSIGHTS_GROUP_LABEL)
    end

    def has_team_label?
      Labels::THREAT_INSIGHTS_TEAM_LABELS.any? { |label| event.label_names.include?(label) }
    end

    def remove_team_labels
      body = "/unlabel #{Labels::THREAT_INSIGHTS_TEAM_LABELS.map { |l| %(~"#{l}") }.join(' ')}"
      add_comment(body, append_source_link: false)
    end
  end
end
