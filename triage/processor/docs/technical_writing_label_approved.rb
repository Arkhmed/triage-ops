# frozen_string_literal: true

require_relative 'technical_writing_label_base'

module Triage
  module Docs
    class TechnicalWritingLabelApproved < TechnicalWritingLabelBase
      react_to_approvals

      def documentation
        <<~TEXT
          This processor ensures the 'Technical Writing' label is applied
          to merge requests approved by members of the Docs team.
        TEXT
      end
    end
  end
end
