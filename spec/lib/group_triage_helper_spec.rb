# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/group_triage_helper'

class DummyGroupTriageHelper
  include GroupTriageHelperContext

  def titled_heatmap(title)
    "Stub heatmap with title: #{title}"
  end
end

RSpec.describe DummyGroupTriageHelper do
  describe "#build_command" do
    let(:strings) { %w[user1 user2 user3] }

    context 'with no flags' do
      subject { described_class.new.build_command(strings) }

      it 'returns the correct string' do
        expect(subject).to eq("user1 user2 user3")
      end
    end

    context 'with a prefix' do
      subject { described_class.new.build_command(strings, prefix: 'hey-') }

      it 'returns the correct string' do
        expect(subject).to eq("hey-user1 hey-user2 hey-user3")
      end
    end

    context 'with the quote flag' do
      subject { described_class.new.build_command(strings, quote: true) }

      it 'returns the correct string' do
        expect(subject).to eq("\"user1\" \"user2\" \"user3\"")
      end
    end

    context 'with the backticks flag' do
      subject { described_class.new.build_command(strings, backticks: true) }

      it 'returns the correct string' do
        expect(subject).to eq("`user1` `user2` `user3`")
      end
    end

    context 'with several flags' do
      subject { described_class.new.build_command(strings, prefix: 'a-', quote: true, backticks: true) }

      it 'returns the correct string' do
        expect(subject).to eq("`a-\"user1\"` `a-\"user2\"` `a-\"user3\"`")
      end
    end
  end

  describe "#build_mentions" do
    let(:assignees) { %w[user1 user2 user3] }

    subject { described_class.new.build_mentions(assignees) }

    it 'returns a string of assignees' do
      expect(subject).to eq("@user1 @user2 @user3")
    end
  end

  describe "#build_backticked_mentions" do
    let(:assignees) { %w[user1 user2 user3] }

    subject { described_class.new.build_backticked_mentions(assignees) }

    it 'returns a string of backticked assignees' do
      expect(subject).to eq("`@user1` `@user2` `@user3`")
    end
  end

  describe "#missed_slo_heatmap" do
    it 'returns a heatmap titled as Heatmap for ~SLO::Missed bug' do
      expect(described_class.new.missed_slo_heatmap).to eq(
        "Stub heatmap with title: ### Heatmap for ~SLO::Missed bugs"
      )
    end
  end

  describe "#merge_group" do
    include GroupDefinition

    let(:assignees) { nil }

    subject { described_class.new.merge_group(group_contributor_success, assignees: assignees) }

    context 'when provided assignees is nil' do
      it 'keeps the same assignees' do
        expect(subject[:assignees]).to match_array(group_contributor_success[:assignees])
      end
    end

    context 'when provided assignees is not nil' do
      let(:assignees) { ["@taucher2003", "@zillemarco"] }

      it 'merges the original assignees with the provided ones' do
        expect(subject[:assignees]).to match_array(
          group_contributor_success[:assignees] + ["@taucher2003", "@zillemarco"])
      end
    end
  end
end
