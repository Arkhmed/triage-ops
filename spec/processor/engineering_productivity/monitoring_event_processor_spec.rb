# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/engineering_productivity/monitoring_event_processor'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::MonitoringEventProcessor do
  let(:event) { instance_double(Triage::MonitoringEvent) }

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['monitoring.uptime_check']

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'calls logger.info' do
      expect(SuckerPunch.logger).to receive(:info)

      subject.process
    end
  end
end
