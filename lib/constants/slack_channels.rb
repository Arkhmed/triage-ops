# frozen_string_literal: true

module SlackChannels
  GROUP_LABEL_TO_CHANNEL_MAP = {
    'group::acquisition' => 'g_acquisition',
    'group::authentication and authorization' => 'g_manage_auth',
    'group::pipeline security' => 'g_pipeline-security',
    'group::pipeline authoring' => 'g_pipeline-authoring_alerts',
    'group::pipeline execution' => 'g_pipeline-execution',
    'group::project management' => 'g_project-management',
    'group::environments' => 'g_environments'
    # gradual rollout:
    # 'group::import and integration' => 'g_manage_import_and_integrate',
    # 'group::foundations' => 'g_manage_foundations',
    # 'group::product planning' => 'g_product-planning',
    # 'group::knowledge' => 'g_knowledge',
    # 'group::optimize' => 'g_plan_optimize',
    # 'group::source code' => 'g_create_source-code',
    # 'group::code review' => 'g_create_code-review',
    # 'group::ide' => 'g_create_ide',
    # 'group::runner' => 'g_runner',
    # 'group::runner saas' => 'g_runner_saas',
    # 'group::package registry' => 'g_package-registry',
    # 'group::container registry' => 'g_container-registry',
    # 'group::static analysis' => 'g_secure-static-analysis',
    # 'group::dynamic analysis' => 'g_secure-dynamic-analysis',
    # 'group::composition analysis' => 'g_secure-composition-analysis',
    # 'group::vulnerability research' => 'g_secure-vulnerability-research',
    # 'group:anti-abuse' => 'g_anti-abuse',
    # 'group::respond' => 'g_respond',
    # 'group::observability' => 'g_observability',
    # 'group::security policies' => 'g_govern_security_policies',
    # 'group::threat insights' => 'g_govern_threat_insights',
    # 'group::compliance' => 'g_govern_compliance',
    # 'group::analytics instrumentation' => 'g_analyze_analytics_instrumentation',
    # 'group::product analytics' => 'g_analyze_product_analytics',
    # 'group::purchase' => 'g_purchase',
    # 'group::provision' => 'g_provision',
    # 'group::utilization' => 'g_utilization',
    # 'group::fulfillment platform' => 'g_fulfillment_platform',
    # 'group::billing and subscription management' => 'g_billing_and_subscription_management',
    # 'group::activation' => 'g_activation'
  }.freeze
end
