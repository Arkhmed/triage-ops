.common_conditions: &common_conditions
  state: opened
  forbidden_labels:
    - type::bug
    - "quad-planning::complete-action"
    - "quad-planning::complete-no-action"
  milestone:
    - Any
  ruby: |
    labels_expected = ['workflow::ready for development', 'workflow::in dev', 'quad-planning::ready']
    /^\d+\.\d+$/ =~ milestone&.title && labels_expected.any? { |label| resource[:labels].include?(label) }

.common_summary: &common_summary
  item: |
    - [ ] #{resource[:web_url]} {{title}} {{labels}} {{milestone}}
  summary: |
    # Group: {{title}}
    {{items}}
.common_actions: &common_actions
  label:
    - "quad-planning::ready"

resource_rules:
  issues:
    summaries:
      - name: Quad Planning Issues for Dev
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Dev
            summary: |
              Hi @gl-quality/dev-qe :wave:

              Please quad-plan the following issues:

              {{items}}

              ---

              Job URL: #{ENV['CI_JOB_URL']}

              This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/quad-planning-issues.yml)

              /label ~"section::dev" ~"triage report" ~Quality
              /assign @gl-quality/dev-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::source code"
            conditions:
              <<: *common_conditions
              labels:
                - "group::source code"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::source code"
          - name: Quad Planning Issues for ~"group::code review"
            conditions:
              <<: *common_conditions
              labels:
                - "group::code review"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::code review"
          - name: Quad Planning Issues for ~"group::ide"
            conditions:
              <<: *common_conditions
              labels:
                - "group::ide"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::ide"
          - name: Quad Planning Issues for ~"group::authentication and authorization"
            conditions:
              <<: *common_conditions
              labels:
                - "group::authentication and authorization"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::authentication and authorization"
          - name: Quad Planning Issues for ~"group::import and integrate"
            conditions:
              <<: *common_conditions
              labels:
                - "group::import and integrate"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::import and integrate"
          - name: Quad Planning Issues for ~"group::optimize"
            conditions:
              <<: *common_conditions
              labels:
                - "group::optimize"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::optimize"
      - name: Quad Planning Issues for Ops Section
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Ops
            summary: |
              Hi @gl-quality/ops-qe :wave:

              Please quad-plan the following issues:

              {{items}}

              ---

              Job URL: #{ENV['CI_JOB_URL']}

              This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/quad-planning-issues.yml)

              /label ~"section::ops" ~"triage report" ~Quality
              /assign @gl-quality/ops-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::runner"
            conditions:
              <<: *common_conditions
              labels:
                - "group::runner"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::runner"
          - name: Quad Planning Issues for ~"group::runner saas"
            conditions:
              <<: *common_conditions
              labels:
                - "group::runner saas"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::runner saas"
          - name: Quad Planning Issues for ~"group::package registry"
            conditions:
              <<: *common_conditions
              labels:
                - "group::package registry"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::package registry"
          - name: Quad Planning Issues for ~"group::container registry"
            conditions:
              <<: *common_conditions
              labels:
                - "group::container registry"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::container registry"
          - name: Quad Planning Issues for ~"group::pipeline execution"
            conditions:
              <<: *common_conditions
              labels:
                - "group::pipeline execution"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::pipeline execution"
          - name: Quad Planning Issues for ~"group::pipeline authoring"
            conditions:
              <<: *common_conditions
              labels:
                - "group::pipeline authoring"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::pipeline authoring"
      - name: Quad Planning Issues for Enablement and SaaS Platforms
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Enablement and SaaS Platforms
            summary: |
              Hi @gl-quality/enablement-platforms-qe :wave:

              Please quad-plan the following issues:

              {{items}}

              ---

              Job URL: #{ENV['CI_JOB_URL']}

              This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/quad-planning-issues.yml)

              /label ~"section::enablement" ~"triage report" ~Quality
              /assign @gl-quality/enablement-platforms-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::distribution"
            conditions:
              <<: *common_conditions
              labels:
                - "group::distribution"
              ruby: |
                labels_expected = ['Deliverable', 'quad-planning::ready']
                /^\d+\.\d+$/ =~ milestone&.title && labels_expected.any? { |label| resource[:labels].include?(label) }
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::distribution"
          - name: Quad Planning Issues for ~"group::geo"
            conditions:
              <<: *common_conditions
              labels:
                - "group::geo"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::geo"
          - name: Quad Planning Issues for ~"group::gitaly"
            conditions:
              <<: *common_conditions
              labels:
                - "group::gitaly"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::gitaly"
          - name: Quad Planning Issues for ~"group::tenant scale"
            conditions:
              <<: *common_conditions
              labels:
                - "group::tenant scale"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::tenant scale"
          - name: Quad Planning Issues for ~"group::pubsec services"
            conditions:
              <<: *common_conditions
              labels:
                - "group::pubsec services"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::pubsec services"
          - name: Quad Planning Issues for ~"group::dedicated"
            conditions:
              <<: *common_conditions
              labels:
                - "group::dedicated"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::dedicated"
      - name: Quad Planning Issues for Fulfillment
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Fulfillment
            summary: |
              Hi @gl-quality/fulfillment-qe :wave:

              Please quad-plan the following issues:

              {{items}}

              ---

              Job URL: #{ENV['CI_JOB_URL']}

              This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/quad-planning-issues.yml)

              /label ~"section::fulfillment" ~"triage report" ~Quality
              /assign @gl-quality/fulfillment-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::fulfillment platform"
            conditions:
              <<: *common_conditions
              labels:
                - "group::fulfillment platform"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::fulfillment platform"
          - name: Quad Planning Issues for ~"group::purchase"
            conditions:
              <<: *common_conditions
              labels:
                - "group::purchase"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::purchase"
          - name: Quad Planning Issues for ~"group::billing and subscription management"
            conditions:
              <<: *common_conditions
              labels:
                - "group::billing and subscription management"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::billing and subscription management"
          - name: Quad Planning Issues for ~"group::utilization"
            conditions:
              <<: *common_conditions
              labels:
                - "group::utilization"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::utilization"
          - name: Quad Planning Issues for ~"group::provision"
            conditions:
              <<: *common_conditions
              labels:
                - "group::provision"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::provision"
      - name: Quad Planning Issues for Sec
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Sec
            summary: |
              Hi @gl-quality/sec-qe :wave:

              Please quad-plan the following issues:

              {{items}}

              ---

              Job URL: #{ENV['CI_JOB_URL']}

              This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/quad-planning-issues.yml)

              /label ~"section::sec" ~"triage report" ~Quality
              /assign @gl-quality/sec-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::compliance"
            conditions:
              <<: *common_conditions
              labels:
                - "group::compliance"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::compliance"
          - name: Quad Planning Issues for ~"group::composition analysis"
            conditions:
              <<: *common_conditions
              labels:
                - "group::composition analysis"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::composition analysis"
          - name: Quad Planning Issues for ~"group::static analysis"
            conditions:
              <<: *common_conditions
              labels:
                - "group::static analysis"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::static analysis"
          - name: Quad Planning Issues for ~"group::threat insights"
            conditions:
              <<: *common_conditions
              labels:
                - "group::threat insights"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::threat insights"
      - name: Quad Planning Issues for Data Science
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Data Science
            summary: |
              Hi @mlapierre :wave:

              Please quad-plan the following issues:

              {{items}}

              ---

              Job URL: #{ENV['CI_JOB_URL']}

              This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/quad-planning-issues.yml)

              /label ~"section::data-science" ~"triage report" ~Quality
              /assign @mlapierre
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::applied ml"
            conditions:
              <<: *common_conditions
              labels:
                - "group::applied ml"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::applied ml"
