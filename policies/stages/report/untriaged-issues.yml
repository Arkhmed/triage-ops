.common_conditions: &common_conditions
  date:
    attribute: created_at
    condition: newer_than
    interval_type: days
    interval: 1
  state: opened
  forbidden_labels:
    - automation:self-triage-encouraged
  author_member:
    source: group
    condition: not_member_of
    source_id: 9970
  ruby: |
    resource[:issue_type] == 'issue' &&
      !bot_author? &&
      untriaged?

resource_rules:
  issues:
    rules:
      - name: Encourage the self triage of untriaged issues
        conditions:
          <<: *common_conditions
        actions:
          redact_confidential_resources: false
          comment: |
            Hey {{author}}, thank you for creating this issue!

            To get the right eyes on your issue more quickly, we encourage you to follow the
            [issue triage best practices](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#partial-triage-checklist).

            **PLEASE NOTE**: You will need to use the
            [`@gitlab-bot label command`](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#reactive-label-and-unlabel-commands)
            to apply labels to this issue.

            To set expectations, GitLab product managers or team members can't make any promise if they will proceed with this.
            However, we believe [everyone can contribute](https://about.gitlab.com/company/mission/#everyone-can-contribute),
            and welcome you to work on this proposed change, feature or bug fix.
            There is a [bias for action](https://about.gitlab.com/handbook/values/#bias-for-action),
            so you don't need to wait. Try and spin up that merge request yourself.

            If you need help doing so, we're always open to [mentor you](https://about.gitlab.com/community/contribute/mentor-sessions/)
            to drive this change.

            *This message was [generated automatically](https://about.gitlab.com/handbook/engineering/quality/triage-operations).
            You're welcome to [improve it](#{ENV['CI_PROJECT_URL']}/-/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/untriaged-issues.yml).*

            /label ~automation:self-triage-encouraged
